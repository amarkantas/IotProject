// Librairies à inclure
#include <SPI.h>
#include <SFE_CC3000.h>
#include <SFE_CC3000_Client.h>
#include <Wire.h>
#include "Ambient2.h"


// Déclaration des pins
#define CC3000_INT      3   // Pin d'interruption (2 pour le slot 1 / 3 pour le slot 2)
#define CC3000_EN       47  // Pin d'activation (14 pour le slot 1 / 15 pour le slot 2)
#define CC3000_CS       45   // Pin de sélection (10 pour le slot 1 / 9 pour le slot 2)
// Déclaration des constantes
#define IP_ADDR_LEN     4   // Longueur des addresses IP en octets
#define MAC_ADDR_LEN    6   // Length of MAC address in bytes


// Création de l'objet SFE_CC3000
SFE_CC3000 wifi = SFE_CC3000(CC3000_INT, CC3000_EN, CC3000_CS);
SFE_CC3000_Client client = SFE_CC3000_Client(wifi);

Opt3001 Ambient2;

// WIFI
char ap_ssid[] = "LAFQAP";                  // SSID du réseau LAFQAP
char ap_password[] = "passwordAA";          // Mot de passe du réseau
uint8_t macAddress[6] = { 0x08, 0x00, 0x20, 0x05, 0x79, 0xB2 };
unsigned int ap_security = WLAN_SEC_WPA2;   // Type de sécurité du réseau
unsigned int timeout = 30000;               // Délai de timeout en millisecondes
IPAddress remote_ip;
unsigned int num_pings = 3;                 // Nombre de ping
IPAddress ip_addr;

//IPAddress remote_ip;
PingReport ping_report = {0};


float reading;

// Detection mouvment
int inputPin = 11;               // choose the input pin (for PIR sensor)
int pirState = LOW;             // we start, assuming no motion detected
int val = 0;                    // variable for reading the pin status

// Light
int light_old=0;
int light=0;
int mouvement=0;
int mouvement_old=0;


char dataSend[20];
String data;
  
void setup() {
  Serial.begin(115200);
  Serial.println(" _____    ____    _______     _____    _____     ____         _   ______    _____   _______ ");
  Serial.println("|_   _|  / __ \\  |__   __|   |  __ \\  |  __ \\   / __ \\       | | |  ____|  / ____| |__   __|");
  Serial.println("  | |   | |  | |    | |      | |__) | | |__) | | |  | |      | | | |__    | |         | |   ");
  Serial.println("  | |   | |  | |    | |      |  ___/  |  _  /  | |  | |  _   | | |  __|   | |         | |   ");
  Serial.println(" _| |_  | |__| |    | |      | |      | | \\ \\  | |__| | | |__| | | |____  | |____     | |   ");
  Serial.println("|_____|  \\____/     |_|      |_|      |_|  \\_\\  \\____/   \\____/  |______|  \\_____|    |_|   ");
       
                                                                                            
  remote_ip[0]=192;
  remote_ip[1]=168;
  remote_ip[2]=4;
  remote_ip[3]=1;

  unsigned char mac_addr[MAC_ADDR_LEN];

  // Start communication with OPT3001
  
  Serial.println("Initializing...");
  // Start I2C communication
  Wire.begin();  
  initAmbient();
  initWifi();
  initDetectMouv();
  scanNetworks();
  connectWifi();

}

// ----------- INIT FUNCTION  ----------- 

void initAmbient(){
  Ambient2.begin(); 
  Ambient2.start_continuous();
}

void initWifi(){
  // Initialisation du module WIFI (Communication SPI)
  if (wifi.init()) {
    Serial.println("Initialisation complete du module WIFI");
  } else {
    Serial.println("ERREUR : L'initialisation du module a échoue");
  }
}

void setMacAddress(){
   /* Optional: Update the Mac Address to a known value */

   if (!wifi.setMacAddress(macAddress))
   {
     Serial.println(F("Failed trying to update the MAC address"));
     while(1);
   }
}

void initDetectMouv(){
    pinMode(inputPin, INPUT);     // declare sensor as inpu 
}

// ----------- GET MEASUREMENT ----------- //

float getLight() {
  if (Ambient2.is_Overflow()){
     Serial.println("Overflow detected"); 
  }
  else{
     // Read the measurement
     reading = Ambient2.measure_Lux();
  }
  return reading;
}

bool getDetect() {
  val = digitalRead(inputPin);                // Lit la valeur du capteur
  if (val == HIGH) {                          // Si le cateur détecte une présence
    if (pirState == LOW) {                    // Vérifie si l'état précédent était Bas
      pirState = HIGH;                        // Fixe la valeur de l'état précédent à Haut
    }
  } else {
    if (pirState == HIGH) {                   // Vérifie si l'état précédent était Haut
      pirState = LOW;                         // Fixe la valeur de l'état précédent à Bas
    }
  }
  return val;                                 // Retourne la valeur du capteur
}


// ----------- NETWORK FUNCTION ----------- //

void connectWifi() {
  Serial.print("Connexion au reseau ");
  Serial.println(ap_ssid);
  if (!wifi.connect(ap_ssid, ap_security, ap_password, timeout)) {
    Serial.println("ERREUR : Impossible de se connecter au réseau");
  }
}

void scanNetworks() {
  int i;
  AccessPointInfo ap_info;
  // Perform scan of nearby WAPs
  Serial.println("Scanning APs. Waiting for scan to complete.");
  if ( wifi.scanAccessPoints(500) != true ) {
    Serial.println("Error scanning APs");
  }

  // Iterate through available WAPs and print their information
  Serial.println("Access Points found:");
  Serial.println();
  while ( wifi.getNextAccessPoint(ap_info) ) {
    Serial.print("SSID: ");
    Serial.println(ap_info.ssid);
    Serial.print("MAC address: ");
    for ( i = 0; i < BSSID_LENGTH; i++ ) {
      if ( ap_info.bssid[i] < 0x10 ) {
        Serial.print("0");
      }
      Serial.print(ap_info.bssid[i], HEX);
      if ( i < BSSID_LENGTH - 1 ) {
        Serial.print(":");
      }
    }
    Serial.println();
    Serial.print("RSSI: ");
    Serial.println(ap_info.rssi, DEC);
    Serial.print("Security: ");
    switch (ap_info.security_mode) {
      case WLAN_SEC_UNSEC:
        Serial.println("Unsecured");
        break;
      case WLAN_SEC_WEP:
        Serial.println("WEP");
        break;
      case WLAN_SEC_WPA:
        Serial.println("WPA");
        break;
      case WLAN_SEC_WPA2:
        Serial.println("WPA2");
        break;
      default:
        break;
    }
    Serial.println();
  }
}



void loop() {
  light=getLight();
  if(light > light_old +10 || light < light_old -10){
    
    light_old=light;
    if(!client.connect(remote_ip, 8080)){
      Serial.println("Error: Could not make a TCP connection");
    }else{
      data=((String)"Light-"+light);
      data.toCharArray(dataSend,data.length()+1);
      client.print(dataSend);
      Serial.println(dataSend);
      client.close();
    }
  } 

  if(getDetect() && mouvement_old == 0){ // Si il y a eu un changement
     if(!client.connect(remote_ip, 8080)){
      Serial.println("Error: Could not make a TCP connection");
    }
    client.println("Mouvement-1");
    client.close();
    Serial.println("Mouvement detecte");
    mouvement_old=1;
  }else if(!getDetect() && mouvement_old == 1){ // Si il y a eu un changement
         if(!client.connect(remote_ip, 8080)){
      Serial.println("Error: Could not make a TCP connection");
    }
    client.println("Mouvement-0");
    client.close();
    Serial.println("Mouvement arrete");
    mouvement_old=0;
  }
  delay(1000);
}
