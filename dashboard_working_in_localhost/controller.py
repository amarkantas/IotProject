from flask import Flask,render_template
from flaskext.mysql import MySQL
from flask.ext.cors import CORS, cross_origin
import request 
import time
import datetime
import json
import bottle
from bottle import response



app = bottle.app()
#app = Flask(__name__, template_folder='views')
app = Flask(__name__)


mysql = MySQL()
app = Flask(__name__)
app.config['MYSQL_DATABASE_USER'] = 'iot'
app.config['MYSQL_DATABASE_PASSWORD'] = 'w1zmQAQ9ENk70pF6'
app.config['MYSQL_DATABASE_DB'] = 'iot'
app.config['MYSQL_DATABASE_HOST'] = 'naskantas.myqnapcloud.com'
mysql.init_app(app)

if __name__ == "__main__":
    app.run(host="0.0.0.0",port=80)






app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy   dog'
app.config['CORS_HEADERS'] = 'Content-Type'

cors = CORS(app, resources={r"/get_data/QLAF/Distance": {"origins": "http://127.0.0.1:5000"}})
cors = CORS(app, resources={r"/get_data/QLAF/Mouvement": {"origins": "http://127.0.0.1:5000"}})
cors = CORS(app, resources={r"/get_data/QLAF/Light": {"origins": "http://127.0.0.1:5000"}})

   
@cross_origin(origin='127.0.0.1',headers=['Content- Type','Authorization']) 

@app.route('/detector/register/<detector_name>', methods = ['GET'])
def registre_detector(detector_name):
     
     cursor = mysql.connect().cursor()
     
     data  = [detector_name]

     #check if this name detector is available:
     cursor.execute("""SELECT * FROM groupe_detector WHERE groupe_detector_name=%s""",(detector_name,))

     if cursor.rowcount  != 0:

    	return "ERROR this detector name is not available!"

    
     else:

    	 cursor.execute("insert into groupe_detector (groupe_detector_id,groupe_detector_name)  values(default,%s)", data)
     

     	 return "Detector registration ok"



@app.route('/groupe_detector/<detector_name>/set_detector/<detector_function>/<detector_data>', methods = ['GET'])
def set_detector(detector_name,detector_function,detector_data):
    
    cursor=mysql.connect().cursor()

    cursor.execute("""SELECT * FROM groupe_detector WHERE groupe_detector_name=%s""",(detector_name,))

    data = cursor.fetchall()
    
    #print(data)

    if cursor.rowcount  != 0:

   
    	for row in data:

   	    print row[0], row[1]
   	    index_groupe_detector=row[0]
  	    data_detector_name=detector_function
   	    data=detector_data.strip()
   	    params=[index_groupe_detector,data_detector_name,data]
   	    cursor.execute("insert into data_detector (id_data_detector,index_groupe_detector,data_detector_name,data)  values(default,%s,%s,%s)", params)


   	return "Data saved!"
    
    else:

    	 return "this detector doesn't exist in database"
    
   
@app.route('/get_data/<groupe_name>/<detector_name>')
def get_data(groupe_name,detector_name):

    cursor = mysql.connect().cursor()
    data = {}
    #list detector recorded:
    cursor.execute("""SELECT  data, log FROM groupe_detector 
                      INNER JOIN data_detector ON groupe_detector.groupe_detector_id = data_detector.index_groupe_detector
                      WHERE groupe_detector_name =  %s AND data_detector_name =%s  """,(groupe_name,detector_name,))
    data_list_detector_for_this_detector = cursor.fetchall()
    
    for line in data_list_detector_for_this_detector:
    	date = line[1].strftime("%d/%m/%Y %H:%M:%S")
    	data[date] = line[0]
    return json.dumps(data)


@app.route('/')
def dashboard():

    cursor = mysql.connect().cursor()
    data = {}
    #list detector recorded:
    cursor.execute("""SELECT groupe_detector_id,groupe_detector_name FROM groupe_detector""")
    data_list_detector = cursor.fetchall()

    #foreach groupe_detector get their detector:
    for row in data_list_detector:
        data[row[1]] = {}
        index_groupe_detector=row[0]

        cursor.execute("""SELECT * FROM data_detector
        				  WHERE (index_groupe_detector,data_detector_name,log) IN (
						  	SELECT index_groupe_detector,data_detector_name,max(log) FROM data_detector
                          	WHERE index_groupe_detector=%s
                          	GROUP BY index_groupe_detector,data_detector_name
                          )
                          """,(index_groupe_detector,))

        data_detector = cursor.fetchall()
        print data_detector 
        for detector in data_detector:
          data[row[1]][detector[2]] = {}
          data[row[1]][detector[2]]["date"]=detector[4]
          data[row[1]][detector[2]]["value"]=detector[3]
      
    return render_template('index.html',variable2=data_list_detector,variable3=data_detector,variable4=data)
    
